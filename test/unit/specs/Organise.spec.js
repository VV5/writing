import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Organise from '@/components/Organise'
import Category from '@/components/Category'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Organise', () => {
  let wrapper, store, state

  beforeEach(() => {
    state = {
      cards: [
        { id: 1, selected: false },
        { id: 2, selected: false }
      ]
    }

    store = new Vuex.Store({
      state
    })

    wrapper = shallowMount(Organise, {
      attachToDocument: true,
      store,
      localVue
    })
  })

  test('should increase match count when a correct image is matched to the category', () => {
    expect(wrapper.vm.matchCount).toBe(0)

    wrapper.find(Category).vm.$emit('increaseMatchCount')

    expect(wrapper.vm.matchCount).toBe(1)
  })

  test('should navigate to poem when button is clicked', () => {
    const $route = {
      path: '/poem'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Organise, {
      store,
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    wrapper.find('.goto-animated-video').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
