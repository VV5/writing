import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper

  test('should navigate to storyboard correctly', () => {
    const $route = {
      path: '/animated-video/1'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      router, localVue
    })

    wrapper.find('.start-button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
