import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Card from '@/components/Card'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Card', () => {
  let wrapper, store, mutations

  beforeEach(() => {
    mutations = {
      setCardSelected: jest.fn()
    }

    store = new Vuex.Store({
      mutations
    })

    wrapper = shallowMount(Card, {
      store,
      localVue,
      propsData: {
        card: { id: 1, selected: false }
      }
    })
  })

  test('should reveal cards when clicked', () => {
    wrapper.find('.is-card').trigger('click')

    expect(mutations.setCardSelected).toHaveBeenCalled()
  })
})
