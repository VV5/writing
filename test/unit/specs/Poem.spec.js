import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Poem from '@/components/Poem'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Poem', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Poem, {
      store, router, localVue
    })
  })
  test('should increase postCounter correctly when audio has ended playing', () => {
    expect(wrapper.vm.postCounter).toBe(1)

    wrapper.find('audio').trigger('ended')

    expect(wrapper.vm.postCounter).toBe(2)
    expect(wrapper.vm.showNextPost).toBe(true)
  })

  test('should not increase postCounter after it has reached 2', () => {
    wrapper.setData({ postCounter: 2 })

    expect(wrapper.vm.postCounter).toBe(2)

    wrapper.find('audio').trigger('ended')

    expect(wrapper.vm.postCounter).toBe(2)
  })

  test('should navigate to the animated video correctly', () => {
    const $route = {
      path: '/animated-video/2'
    }

    wrapper.find('.goto-animated-video').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
