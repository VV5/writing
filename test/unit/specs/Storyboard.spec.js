import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Storyboard from '@/components/Storyboard'
import Card from '@/components/Card'
import CardModal from '@/components/CardModal'
import store from '@/store'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Storyboard', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(Storyboard, {
      store, router, localVue
    })
  })

  test('should open modal when one of the cards is clicked', () => {
    wrapper.find(Card).vm.$emit('openModal')
    wrapper.vm.$emit('openModal')

    expect(wrapper.emitted().openModal).toBeTruthy()
    expect(wrapper.vm.isModalActive).toBe(true)
  })

  test('should flip the card when the card has been selected', () => {
    wrapper.find(Card).vm.$emit('setFlippedCard')
    wrapper.vm.$emit('setFlippedCard')

    expect(wrapper.emitted().setFlippedCard).toBeTruthy()
  })

  test('should close the modal properly', () => {
    wrapper.setData({ isModalActive: true })

    expect(wrapper.vm.isModalActive).toBe(true)

    wrapper.find(CardModal).vm.$emit('closeModal')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
    expect(wrapper.vm.isModalActive).toBe(false)
  })

  test('should navigate to organise correctly when button is clicked', () => {
    const $route = { path: '/organise' }

    wrapper = shallowMount(Storyboard, {
      store,
      router,
      localVue,
      computed: { showNextButton: () => true }
    })

    wrapper.find('.goto-organise').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
