import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import CardModal from '@/components/CardModal'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('CardModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(CardModal, {
      attachToDocument: true,
      propsData: {
        flippedCard: { id: 1 },
        isModalActive: true
      }
    })
  })

  test('should close the modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
