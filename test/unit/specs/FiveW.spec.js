import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import FiveW from '@/components/FiveW'
import FiveModal from '@/components/FiveModal'
import Dropzone from '@/components/Dropzone'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('FiveW', () => {
  let wrapper
  let router

  beforeEach(() => {
    router = new VueRouter()

    wrapper = shallowMount(FiveW, {
      attachToDocument: true,
      router,
      localVue
    })

    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should increase matchCount when button is clicked', () => {
    expect(wrapper.vm.matchCount).toBe(0)

    wrapper.find(Dropzone).vm.$emit('increaseMatchCount')
    wrapper.vm.$emit('increaseMatchCount')

    expect(wrapper.emitted().increaseMatchCount).toBeTruthy()
    expect(wrapper.vm.matchCount).toBe(1)
  })

  test('should not increase matchCount and set final to true', () => {
    expect(wrapper.vm.final).toBe(false)

    wrapper.setData({ matchCount: wrapper.vm.questions.length - 1 })

    wrapper.find(Dropzone).vm.$emit('increaseMatchCount')
    wrapper.vm.$emit('increaseMatchCount')

    expect(wrapper.emitted().increaseMatchCount).toBeTruthy()
    expect(wrapper.vm.final).toBe(true)
  })

  test('should close the modal correctly', () => {
    wrapper.setData({ isActive: true })
    expect(wrapper.vm.isActive).toBe(true)

    wrapper.find(FiveModal).vm.$emit('closeModal')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should navigate to mix and match correctly', () => {
    const $route = {
      path: '/mix-match'
    }

    const router = new VueRouter()

    wrapper = shallowMount(FiveW, {
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    wrapper.find('.goto-mix-match').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
