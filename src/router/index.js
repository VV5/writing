import Vue from 'vue'
import Router from 'vue-router'
import Copyright from '@/components/Copyright'
import Introduction from '@/components/Introduction'
import AnimatedVideo from '@/components/AnimatedVideo'
import Storyboard from '@/components/Storyboard'
import Organise from '@/components/Organise'
import Middle from '@/components/MiddleSplash'
import Poem from '@/components/Poem'
import FiveW from '@/components/FiveW'
import MixMatch from '@/components/MixMatch'
import Credits from '@/components/Credits'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'Copyright',
      component: Copyright
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction
    },
    {
      path: '/animated-video/:id',
      name: 'AnimatedVideo',
      component: AnimatedVideo
    },
    {
      path: '/storyboard',
      name: 'Storyboard',
      component: Storyboard
    },
    {
      path: '/organise',
      name: 'Organise',
      component: Organise
    },
    {
      path: '/middle',
      name: 'Middle',
      component: Middle
    },
    {
      path: '/poem',
      name: 'Poem',
      component: Poem
    },
    {
      path: '/five-w',
      name: 'FiveW',
      component: FiveW
    },
    {
      path: '/mix-match',
      name: 'MixMatch',
      component: MixMatch
    },
    {
      path: '/credits',
      name: 'Credits',
      component: Credits
    }
  ]
})
