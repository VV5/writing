import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    cards: [
      { id: 1, description: 'Trouble using the computer', selected: false, match: 1 },
      { id: 2, description: 'Is lonely', selected: false, match: 2 },
      { id: 3, description: 'Does not connect with children', selected: false, match: 2 },
      { id: 4, description: 'Unable to connect to the internet', selected: false, match: 1 },
      { id: 5, description: 'Start life afresh', selected: false, match: 3 },
      { id: 6, description: 'Nothing to do', selected: false, match: 3 },
      { id: 7, description: 'Persistent in wanting an internet connection', selected: false, match: 1 },
      { id: 8, description: 'Discover new things', selected: false, match: 3 },
      { id: 9, description: 'Has no purpose', selected: false, match: 3 }
    ],
    matchQuestions: [
      {
        id: 1,
        text: 'Who has trouble with the internet?',
        theme: '#89ffb5',
        selected: false

      },
      {
        id: 2,
        text: 'Where is the Speaker?',
        theme: '#ffb6ac',
        selected: false
      },
      {
        id: 3,
        text: 'Why does the mother have trouble?',
        theme: '#70e6ff',
        selected: false
      },
      {
        id: 4,
        text: 'Why did she click open so many windows?',
        theme: '#e14fff',
        selected: false
      },
      {
        id: 5,
        text: 'What will happen if the computer hangs?',
        theme: '#816aff',
        selected: false
      },
      {
        id: 6,
        text: 'What could "finding a connection" meant?',
        theme: '#67b135',
        selected: false
      },
      {
        id: 7,
        text: 'How could she be affected if the computer hangs?',
        theme: '#ff921d',
        selected: false
      },
      {
        id: 8,
        text: 'When will she be able to get help?',
        theme: '#ff385d',
        selected: false
      }
    ],
    matchAnswers: [
      {
        id: 1,
        text: 'Perhaps she does not know to use the computer.',
        theme: '#70e6ff',
        selected: false,
        match: 3
      },
      {
        id: 2,
        text: 'She will likely feel quite helpless.',
        theme: '#ff921d',
        selected: false,
        match: 7
      },
      {
        id: 3,
        text: 'It could refer to the internet connection.',
        theme: '#67b135',
        selected: false,
        match: 6
      },
      {
        id: 4,
        text: 'The speaker is at work so his mother has called him for help.',
        theme: '#ffb6ac',
        selected: false,
        match: 2
      },
      {
        id: 5,
        text: 'She could have kept clicking windows because she did not know what else to do or how to use the computer.',
        theme: '#e14fff',
        selected: false,
        match: 4
      },
      {
        id: 6,
        text: 'The speaker\'s mother has trouble with the internet.',
        theme: '#89ffb5',
        selected: false,
        match: 1
      },
      {
        id: 7,
        text: 'She will likely get more frustrated because she will not be able to do anything anymore.',
        theme: '#816aff',
        selected: false,
        match: 5
      },
      {
        id: 8,
        text: 'When the speaker can assist her.',
        theme: '#ff385d',
        selected: false,
        match: 8
      }
    ],
    correctScreen: false
  },
  mutations: {
    setCardSelected (state, card) {
      const index = state.cards.indexOf(card)

      state.cards[index].selected = true
    },
    correctScreen (state, status) {
      state.correctScreen = status
    },
    setQuestionSelected (state, question) {
      const index = state.matchQuestions.indexOf(question)

      state.matchQuestions[index].selected = true
    },
    setAnswerSelected (state, answer) {
      const index = state.matchAnswers.indexOf(answer)

      state.matchAnswers[index].selected = true
    }
  }
})
